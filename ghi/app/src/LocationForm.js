import React, { useEffect, useState } from 'react';

function LocationForm(props) {

    const [name, setName] = useState('');
    const [roomCount, setRoomCount] = useState('');
    const [city, setCityName] = useState('');
    const [state, setStateName] = useState('');
       // set the usestate hook to store "name" in the components state
        //with a default initial value of an empty string.

        //create the handlenamechange method to take what the user inputs
        //into the form and store it in the states "name" variable
    const handleNameChange = (event) => {
        const value = event.target.value;
            setName(value);
        }

        const handleSubmit = async (event) => {
            event.preventDefault();

            const data = {};
            data.room_count = roomCount;
            data.name = name;
            data.city = city;
            data.state = state;
            console.log(data);

            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
              method: "post",
              body: JSON.stringify(data),
              headers: {
                'Content-Type': 'application/json',
              },
            };

            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
              const newLocation = await response.json();
              console.log(newLocation);

                setName('');
                setRoomCount('');
                setCityName('');
                setStateName('');
            }
          }



//ROOM COUNT

        // set the usestate hook to store "name" in the components state
         //with a default initial value of an empty string.

         //create the handlenamechange method to take what the user inputs
         //into the form and store it in the states "name" variable
     const handleRoomCount = (event) => {
         const value = event.target.value;
             setRoomCount(value);
         }

//CITY
// set the usestate hook to store "name" in the components state
 //with a default initial value of an empty string.

 //create the handlenamechange method to take what the user inputs
 //into the form and store it in the states "name" variable
const handleCity = (event) => {
 const value = event.target.value;
     setCityName(value);
 }


 //STATE

 const handleState = (event) => {
    const value = event.target.value;
        setStateName(value);
    }


    const [states, setStates] = useState([]);
    const fetchData = async () => {
        // MAKE AN API CALL TO THE LIST OF STATES
    const stateURL = 'http://localhost:8000/api/states/';
    // FETCH TEH DATA AT THE LIST STATES URL
    const stateResponse = await fetch(stateURL);
    //IF RESPONSE IS OK
    if (stateResponse.ok) {
        // CHANGE THE RESPONSE TO A JSON FORMATTED DATA RESPONSE
        //ADD AWAIT SO THAT IT DOESNT RETURN THE PROMISE BUT INSTEAD TEH
        //VALUE OF THAT PROMISE WILL TURN INTO AFTER AWAIT
        const stateData = await stateResponse.json();
        setStates(stateData.states)




    }

    // if (response.ok) {
    //     const data = await response.json();
    }

  useEffect(() => {
    fetchData();
  }, []);

        return(
    <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
                <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange}
                    placeholder="Name"
                    required
                    type="text"
                    name="name" value={name}
                    id="name"
                    className="form-control"
                  />
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleRoomCount}
                    placeholder="Room count"
                    required
                    type="number"
                    name="room_count" value={roomCount}
                    id="room_count"
                    className="form-control"
                  />
                  <label htmlFor="room_count">Room count</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleCity}
                    placeholder="City"
                    required
                    type="text"
                    name="city" value={city}
                    id="city"
                    className="form-control"
                  />
                  <label htmlFor="city">City</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleState} required name="state" id="state" value={state} className="form-select">
                    <option value="">Choose a state</option>
                    {states.map(state => {
                    return (
                        <option key={state.abbreviation} value={state.abbreviation}>
                        {state.name}
                        </option>
                        );
                })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
  )
}

export default LocationForm;





// import React from "react";

// function LocationForm() {
//   return (
//     <div className="row">
//       <div className="offset-3 col-6">
//         <div className="shadow p-4 mt-4">
//           <h1>Create a new location</h1>
//           <form id="create-location-form">
//             <div className="form-floating mb-3">
//               <input
//                 placeholder="Name"
//                 required
//                 type="text"
//                 name="name"
//                 id="name"
//                 className="form-control"
//               />
//               <label htmlFor="name">Name</label>
//             </div>
//             <div className="form-floating mb-3">
//               <input
//                 placeholder="Room count"
//                 required
//                 type="number"
//                 name="room_count"
//                 id="room_count"
//                 className="form-control"
//               />
//               <label htmlFor="room_count">Room count</label>
//             </div>
//             <div className="form-floating mb-3">
//               <input
//                 placeholder="City"
//                 required
//                 type="text"
//                 name="city"
//                 id="city"
//                 className="form-control"
//               />
//               <label htmlFor="city">City</label>
//             </div>
//             <div className="mb-3">
//               <select required name="state" id="state" className="form-select">
//                 <option value="">
//                   Choose a state
//                 </option>
//               </select>
//             </div>
//             <button className="btn btn-primary">Create</button>
//           </form>
//         </div>
//       </div>
//     </div>
//   );
// }
// export default LocationForm;
