import Nav from './Nav';
import LocationForm from './LocationForm';
import AttendeesList from './AttendeesList';
import AttendeeConferenceForm from './AttendeeConferenceForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import { BrowserRouter, Routes, Route } from "react-router-dom";


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
    <Nav />
    <div>
      <Routes>
      <Route index element={<MainPage />} />
        <Route path="locations">
          <Route path="new" element={<LocationForm />} />
        </Route>

        <Route path="attendees">
          <Route path="" element={<AttendeesList attendees={props.attendees} />} />
          <Route path="new" element={<AttendeeConferenceForm />} />
        </Route>

        <Route path="conferences">
          <Route path="new" element={<ConferenceForm />} />
          {/* <Route path="new" element={<ConferenceList />} /> */}
        </Route>

        <Route path="presentations">
          <Route path="new" element={<PresentationForm />} />
        </Route>
      </Routes>

      {/* <LocationForm /> */}
      {/* <AttendeesList attendees={props.attendees} /> */}
    </div>
    </BrowserRouter>
  );
}


export default App;
