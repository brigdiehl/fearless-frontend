import React, { useEffect, useState } from 'react';

function PresentationForm(props) {

   //name, email, company name, title, synopsis (choose a conference, create button)
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [company, setCompany] = useState('');
  const [title, setTitle] = useState('');
  const [synopsis, setSynopsis] = useState('');
  const [conference, setConference] = useState('');
     // set the usestate hook to store "name" in the components state
      //with a default initial value of an empty string.

      //create the handlenamechange method to take what the user inputs
      //into the form and store it in the states "name" variable
  const handleNameChange = (event) => {
      const value = event.target.value;
          setName(value);
      }

      const handleSubmit = async (event) => {
          event.preventDefault();

          const data = {};
          data.presenter_email = email;
          data.presenter_name = name;
          data.company_name = company;
          data.title = title;
          data.synopsis = synopsis;
          console.log(data);

          const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`;
          const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },


          };

          const response = await fetch(presentationUrl, fetchConfig);
          if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);

              setName('');
              setEmail('');
              setCompany('');
              setTitle('');
              setSynopsis('');
              setConference('');
          }
        }


   const handleEmail = (event) => {
       const value = event.target.value;
           setEmail(value);
       }

const handleCompany = (event) => {
const value = event.target.value;
   setCompany(value);
}

const handleTitle = (event) => {
  const value = event.target.value;
      setTitle(value);
  }

  const handleSynopsis = (event) => {
    const value = event.target.value;
        setSynopsis(value);
    }

    const handleConferences = (event) => {
      const value = event.target.value;
          setConferences(value);
      }


  const [conferences, setConferences] = useState([]);
  const fetchData = async () => {
  const conferenceURL = 'http://localhost:8000/api/conferences/';
  const stateResponse = await fetch(conferenceURL);
  if (stateResponse.ok) {
      const conferenceData = await stateResponse.json();
      setConferences(conferenceData.conferences)

  }

  }

useEffect(() => {
  fetchData();
}, []);


    return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
                <form onSubmit={handleSubmit} id="create-presentation-form">
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange}
                    placeholder="Name"
                    required
                    type="text"
                    name="name" value={name}
                    id="name"
                    className="form-control"
                  />
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleEmail}
                    placeholder="email"
                    required
                    type="text"
                    name="email" value={email}
                    id="email"
                    className="form-control"
                  />
                  <label htmlFor="email">Email</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleCompany}
                    placeholder="Company"
                    required
                    type="text"
                    name="company" value={company}
                    id="company"
                    className="form-control"
                  />
                  <label htmlFor="company">Company</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleTitle}
                    placeholder="title"
                    required
                    type="text"
                    name="title" value={title}
                    id="title"
                    className="form-control"
                  />
                  <label htmlFor="title">Title</label>
                </div>
                <div className="form-floating mb-3">
                  <textarea onChange={handleSynopsis}
                    placeholder="synopsis"
                    required
                    type="text"
                    name="synopsis" value={synopsis}
                    id="synopsis"
                    className="form-control"
                  />
                  <label htmlFor="email">Synopsis</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleConferences} required name="conference" id="conference" value={conference} className="form-select">
                    <option value="">Choose a conference</option>
                    {conferences.map(conference => {
                    return (
                        <option key={conference.id} value={conference.id}>
                          {conference.name}
                        </option>
                      );
                })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
    )
}

export default PresentationForm;